//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
#include "Array"


int main()
{
    float *floatPointer = nullptr;
    floatPointer = new float[10];
    
    for (int i = 0; i <= 10 ; i++)
    {
        *floatPointer = i;
        ++floatPointer;
    }
    
    for (int i = 0; i <= 10; i++)
    {
        --floatPointer;
    }
    
    for (int i = 0; i <= 10; i ++)
    {
        std::cout << "\n float = " << *floatPointer + i;
    }
    
    delete[] floatPointer;
    
    return 0;
}
